<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aportes extends Model
{
     protected $table    = "aportes";
    protected $fillable = ['titulo', 'descripcion',];
    protected $guarded  = ["id"];
    public $timestamps  = false;
}
