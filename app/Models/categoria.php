<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class categoria extends Model
{
    protected $table    = "categoria";
    protected $fillable = ['nombre', 'descripcion'];
    protected $guarded  = ["id"];
    public $timestamps  = false;

    public function portafolios()
    {
        return $this->hasMany('App\Models\portafolio');
    }
}
