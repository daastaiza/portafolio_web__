<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Secciones extends Model
{
    protected $table    = "secciones";
    protected $fillable = ['nombre', 'descripcion', 'id_portafolio'];
    protected $guarded  = ["id"];
    public $timestamps  = false;

    public function portafolio()
    {

        return $this->belongsTo('App\Models\portafolio', 'id_portafolio', 'id');
    }
}
