<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estudios extends Model
{
    protected $table    = "estudios";
    protected $fillable = ['titulo', 'descripcion','image'];
    protected $guarded  = ["id"];
    public $timestamps  = false;
}
