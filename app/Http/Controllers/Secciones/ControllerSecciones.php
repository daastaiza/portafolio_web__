<?php

namespace App\Http\Controllers\Secciones;

use App\Http\Controllers\Controller;
use App\Models\Portafolio;
use App\Models\Secciones;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class ControllerSecciones extends Controller
{
    public function index()
    {
        $secciones  = Secciones::all();
        $portafolio = Portafolio::orderBy('id', 'ASC')->pluck('nombre', 'id')->toArray();

        return view('secciones.secciones_ss')
            ->with('secciones', $secciones)

            ->with('portafolio', $portafolio);
    }

    public function store(Request $request)
    {
        $secciones = new Secciones($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlog
        $secciones->save();

        $secciones = Secciones::all();

        Flash::success('se registro la informacion');

        return view('secciones.secciones_ss')
            ->with('secciones', $secciones);

    }

    public function edit($id)
    {
        $secciones = Secciones::find($id);

        return view('secciones.editar')->with('secciones', $secciones);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $secciones = Secciones::find($id); //busco el backlog que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $secciones->fill($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlogs
        $secciones->save(); //guardo todo lo que contenga la variable $backlogs

        $secciones = Secciones::all();
        return view('secciones.secciones')->with('secciones', $secciones);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
