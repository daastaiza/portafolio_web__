<?php

namespace App\Http\Controllers\Publicacion;

use App\Http\Controllers\Controller;
use App\Http\Requests\PublicacionRequest;
use App\Models\Publicacion;
use App\Models\Secciones;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class ControllerPublicacion extends Controller
{
    public function index()
    {
        $publicacion = Publicacion::where('id_user', Auth::user()->id)->get();
        //dd($publicacion);
        $publicaciones = Publicacion::all();

        $secciones = Secciones::orderBy('id', 'ASC')->pluck('nombre', 'id')->toArray();

        return view('publicacion.pppublicacion')
            ->with('publicacion', $publicacion)
            ->with('publicaciones', $publicaciones)
            ->with('secciones', $secciones);
    }
    public function general()
    {
        $secciones = Secciones::orderBy('id', 'ASC')->pluck('nombre', 'id')->toArray();
        $publicaciones = Publicacion::all();
        

        return view('portafolios_general')
            ->with('publicaciones', $publicaciones)
            ->with('secciones', $secciones);
            
    }

    public function store(PublicacionRequest $request)
    {
        

        $request->file('image')->store('public');

        $publicacion        = new Publicacion($request->all());
        $publicacion->image = $request->file('image')->store('');
        $publicacion->save();

        $publicacion = Publicacion::where('id_user', Auth::user()->id)->get();
        //dd($publicacion);
        $publicaciones = Publicacion::all();

        $secciones = Secciones::orderBy('id', 'ASC')->pluck('nombre', 'id')->toArray();

        Flash::success('se registro la publicacion');

        return view('publicacion.pppublicacion')
            ->with('publicacion', $publicacion)
            ->with('publicaciones', $publicaciones)
            ->with('secciones', $secciones);
    
    }

    public function edit($id)
    {
        $publicacion = Publicacion::find($id);

        return view('publicaccion.editar')->with('publicacion', $publicacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $publicacion = Publicacion::find($id); //busco el backlog que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $publicacion->fill($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlogs
        $publicacion->save(); //guardo todo lo que contenga la variable $backlogs

        $publicacion = Publicacion::all();
        return view('publicacion.publicaccion')->with('publicacion', $publicacion);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
