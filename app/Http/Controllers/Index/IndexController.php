<?php

namespace App\Http\Controllers\Index;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Informacion;
use App\Models\Estudios;
use Mail;
use Laracasts\Flash\Flash;
use Session;
use Redirect;
class IndexController extends Controller
{
     public function index()
    {
     

        return view('weelcome');
        
    }

    public function estudios()
    {
       
        $estudios = Estudios::all();

        return view('estudios')
        ->with('estudios', $estudios);        
    }

    public function informacion()
    {
       
         $informacion = Informacion::all();

        return view('informacion')
        ->with('informacion', $informacion);        
    }

    public function store(Request $request)
    {

		$informacion = Informacion::all();
        $estudios = Estudios::all();
        Mail::Send('index',$request->all(),function($msj){
        	$msj->subject('Persona interesada en servicios web');
        	$msj->to('pruebasasistenciasena@gmail.com');

        });
        Flash::success('se registro la informacion');
        return view('weelcome')
        ->with('estudios', $estudios)
        ->with('informacion', $informacion);

       
        


    }
}
