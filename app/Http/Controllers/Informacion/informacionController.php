<?php

namespace App\Http\Controllers\Informacion;

use App\Http\Controllers\Controller;
use App\Models\Informacion;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class informacionController extends Controller
{
    public function index()
    {
        $informacion = Informacion::all();

        return view('informacion.informacionn')->with('informacion', $informacion);
    }

    public function store(Request $request)
    {

        $request->file('image')->store('public');

       
        $informacion = new Informacion($request->all()); 
        $informacion->image = $request->file('image')->store('');
        $informacion->save();

        $informacion = Informacion::all();

        Flash::success('se registro la informacion');

        return view('informacion.informacion')->with('informacion', $informacion);

    }

    public function edit($id)
    {
        $informacion = Informacion::find($id);

        return view('informacion.editar')->with('informacion', $informacion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $informacion = Informacion::find($id); //busco el backlog que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $informacion->fill($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlogs
        $informacion->save(); //guardo todo lo que contenga la variable $backlogs

        $informacion = Informacion::all();
        return view('informacion.informacion')->with('informacion', $informacion);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
