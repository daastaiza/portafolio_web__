<?php

namespace App\Http\Controllers\Categoria;

use App\Http\Controllers\Controller;
use App\Models\categoria;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class categoriaController extends Controller
{
    public function index()
    {
        $categoria = Categoria::all();

        return view('categoria.categoria')->with('categoria', $categoria);

    }

    public function store(Request $request)
    {
        $categoria = new Categoria($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlog
        $categoria->save();

        $categoria = Categoria::all();

        Flash::success('se registro la informacion');

        return view('categoria.categoria')->with('categoria', $categoria);

    }

    public function edit($id)
    {
        $categoria = Categoria::find($id);

        return view('categoria.editar')->with('categoria', $categoria);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $categoria = Categoria::find($id); //busco el backlog que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $categoria->fill($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlogs
        $categoria->save(); //guardo todo lo que contenga la variable $backlogs

        $categoria = Informacion::all();
        return view('categoria.categoria')->with('categoria', $categoria);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
