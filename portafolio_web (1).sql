-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2017 a las 23:32:45
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `portafolio_web`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aportes`
--

CREATE TABLE `aportes` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aportes`
--

INSERT INTO `aportes` (`id`, `titulo`, `descripcion`) VALUES
(1, 'Proyecto SCRUM', 'Aporte al proyecto SCRUM en la documentación del proyecto en la construcción de la base de datos y ayuda en las interfaces como en el desarrollo del proyecto en laravel 5.3\r\n\r\nDirección web del proyecto en producción:\r\n\r\n\r\nhttps://www.scrum_proyect.com/\r\n'),
(2, 'Aporte  a inmobiliaria', 'Aporte en el desarrollo de la construcción del aplicativo utilizando java server faces y hibernate para el desarrollo de la funcionalidad del proyecto\r\n\r\nDirección web del proyecto en produccion:\r\nhttps://www.inmobiliaria_fake.com/'),
(3, 'Sistema de gestión de inventario', 'Aporte en la construcción de la base de datos y levantamiento de información\r\n\r\nEn proceso de testeo.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `descripcion`) VALUES
(3, 'tema libre', 'llorelo papa'),
(4, 'tecnologia', 'temas tecnologicos'),
(5, 'educacion', 'cualquier cosa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id`, `nombre`) VALUES
(1, 'bogota'),
(2, 'medellin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudios`
--

CREATE TABLE `estudios` (
  `id` int(11) NOT NULL,
  `titulo` varchar(40) NOT NULL,
  `descripcion` text NOT NULL,
  `image` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estudios`
--

INSERT INTO `estudios` (`id`, `titulo`, `descripcion`, `image`) VALUES
(2, 'Lenguaje de Programación Java', 'Conocimientos Avanzados en lenguaje de programación Java preferiblemente a nivel web Implementando Patrones de diseño y Programacion Orientada a objetos y frameworks como Java Server Faces', '646114132fd289fc6d1c6f824805ac64.png'),
(3, 'Framework Laravel', 'Conocimientos en framework de php \"laravel\" ideal para PyME ya que es un framework de codigo libre que no requiere un pago de una licencia ideal para el desarrollo de un sitio web para tu empresa ! ', '8b4eefb725acc453cc544f8967d7f548.png'),
(4, 'Desarrollo de sistemas de informacion', 'En informática, un sistema de información es cualquier sistema informático que se utilice para obtener, almacenar, manipular, administrar, controlar, procesar, transmitir o recibir datos, para satisfacer una necesidad de información ideal para la optimizacion de un proceso en tu empresa', '6351a24e03b899dbea19f0a4ad74faa4.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informacion`
--

CREATE TABLE `informacion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` text NOT NULL,
  `image` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informacion`
--

INSERT INTO `informacion` (`id`, `nombre`, `descripcion`, `image`) VALUES
(4, 'Diego Alejandro Astaiza Borja', 'Experiencia y conocimientos en lenguajes de programación, como C#, Java, PHP, conocimientos en lenguajes de bases de datos como SQL serve, y MySQL, manipulación de herramientas como GIT, familiarización con entornos de desarrollo como Sublime Text 3, NetBeans, Visual Studio, conocimientos en otros aplicativos como phpMyAdmin, Taiga.io\r\n\r\nSoy una persona activa, me gusta aprender he investigar por mi propia cuenta, me gusta la sensación de satisfacción cuando hago una determinada labor bien hecha y es tomada en cuenta por mi equipo, suelo ser una persona con buen sentido del humor en los momentos oportunos, de lo contrario soy una persona callada y reservada, soy una persona que le gusta llegar a tiempo, ser puntual y organizado con su vida, suelo auto evaluarme para mirar mis defectos, además decirme a mí mismo como puedo mejorar y en ocasiones pregunto a personas de confianza sobre mis errores  y como puedo mejorar y superarme como persona, soy abierto a cualquier tipo de información, ayuda o accesoria que se me quiera dar, para lograr metas en común, como por ejemplo aprender más, soy una persona que considera que el conocimiento es poder, y que mejor manera de cultivar el conocimiento sino es estudiando y poniéndolo en práctica.', 'a2b526b7d5979e5a1afbae1f893d07d6.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portafolio`
--

CREATE TABLE `portafolio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `portafolio`
--

INSERT INTO `portafolio` (`id`, `nombre`, `descripcion`, `id_categoria`) VALUES
(2, 'odiawhdawo', 'hoidwhdaoia', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicaciones`
--

CREATE TABLE `publicaciones` (
  `id` int(11) NOT NULL,
  `id_user` int(10) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `contenido` text NOT NULL,
  `id_seccion` int(11) NOT NULL,
  `image` varchar(150) NOT NULL DEFAULT 'default.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `publicaciones`
--

INSERT INTO `publicaciones` (`id`, `id_user`, `titulo`, `contenido`, `id_seccion`, `image`) VALUES
(1, 2, 'Bizagi', 'La Suite consiste de dos herramientas: Bizagi Studio, el módulo de construcción, y Bizagi BPM Server para ejecución y control. En Bizagi Studio el usuario define el modelo asociado al proceso de negocio (flujograma, reglas de negocio, interfaz de usuario, etc) para la ejecución del mismo. Los modelos se guardan en una base de datos y son utilizados posteriormente en la ejecución por Bizagi BPM Server. Bizagi BPM Server ejecuta un Portal de Trabajo para los usuarios finales en un PC o cualquier dispositivo móvil.\r\n\r\nBizagi BPM Suite tiene varias características como: Seguimiento y monitoreo, alarmas y notificaciones, análisis de desempeño y reportes, auditoría y trazabilidad, enrutamiento de la carga de trabajo y movilidad. Bizagi BPM Suite se puede integrar con sistemas CRM y ERP.', 3, '1e3aa60132605c4b53d3b0dfcc587890.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE `secciones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `id_portafolio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `secciones`
--

INSERT INTO `secciones` (`id`, `nombre`, `descripcion`, `id_portafolio`) VALUES
(3, 'deportes', 'temas relaciones con el deporte', 2),
(4, 'tecnologia', 'temas relacionados con la tecnologia', 2),
(5, 'libres', 'temas libres', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `ciudad`) VALUES
(2, 'Diego Alejando Astaiza Borja', 'daastaiza@misena.edu.co', '$2y$10$K0WAlgmbE.C7SLPHKYzkV.3BNd2zYkakV1hNIYPyGCPE3WXYXo89q', 'iXrEFkETTlQ90x9qa3wgcfDDiIJ638ArYbyTdIwTNX5XEfBvCTLLZu12q2Mw', '2017-10-19 08:51:11', '2017-11-22 03:31:15', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aportes`
--
ALTER TABLE `aportes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estudios`
--
ALTER TABLE `estudios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `informacion`
--
ALTER TABLE `informacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Indices de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_seccion` (`id_seccion`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_portafolio` (`id_portafolio`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `ciudad` (`ciudad`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aportes`
--
ALTER TABLE `aportes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `estudios`
--
ALTER TABLE `estudios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `informacion`
--
ALTER TABLE `informacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `secciones`
--
ALTER TABLE `secciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `portafolio`
--
ALTER TABLE `portafolio`
  ADD CONSTRAINT `portafolio_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
  ADD CONSTRAINT `publicaciones_ibfk_1` FOREIGN KEY (`id_seccion`) REFERENCES `secciones` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `publicaciones_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD CONSTRAINT `secciones_ibfk_1` FOREIGN KEY (`id_portafolio`) REFERENCES `portafolio` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`ciudad`) REFERENCES `ciudad` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
