@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        
                <div class="panel-heading">
                <div class="container">
                    <div class="container">
                    
<h2>Ciudad</h2>
</div>  
                </div>    
                </div>

                <div class="panel-body">
                    <div class="container-fluid">
                        <div class="card">
                <div class="card-header d-flex align-items-center">
                  
                </div>
                <div class="card-block">
                            <div class="panel-body">
                                @if(count($errors) >0)
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                  @include('flash::message')

                                <div class="table-responsive">
                                   <table class="table table-hover table-sm table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Identificador</th>
                                            <th>Nombre</th>
                                            <th>acciones</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($ciudad as $ciudad)
                                        <tr>
                                                <td >{{ $ciudad -> id }}</td>
                                                <td>{{ $ciudad -> nombre}}</td>
                                                <td>
                                                <center>
                                                    <a  class="btn btn-success" href="{{route('Ciudad.edit', $ciudad->id)}}">
                                                        Editar
                                                    </a>
                                                </center>
                                            </td>
                                        </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <a >

                                        <button type="button" class="btn btn-success" data-toggle="modal"   data-target="#Crear">Nueva <i class="fa fa-fw fa-plus"></i></button></a><br><br>
                                </div>
                            </div>
                    </div>
                </div>

                       
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="Crear" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                        <!-- Modal content-->
                                    <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Registro de Peticion</h4>
                                          </div>
                                          <div class="modal-body">


                                        {!! Form::open(['route' => 'Ciudad.store', 'method' => 'POST']) !!}

                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('nombre', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre de la peticion']) !!}
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::submit('Registrar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                                        </div>

                                        {!! Form::close() !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
@endsection
