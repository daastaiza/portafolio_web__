<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                Login
            </title>
        </meta>
        <link href="{{asset('login.css')}}" rel="stylesheet"/>
        <link href="{{asset('assets/css/estiloslogin.css')}}" rel="stylesheet"/>
    </head>
    <style type="text/css">
    </style>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->
                <h2 class="inactive underlineHover">
                    Inicio de


                    Sesion
                </h2>
                <!-- Icon -->
                <div class="fadeIn first">
                    <img alt="User Icon" id="icon" src="images/icon.jpg"/>
                </div>

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Usuario</label>

                            <div class="col-md-6">
                                <input id="login" type="email" class="valor" name="email" value="{{ old('email') }}" required autofocus>


                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="valor" name="password" required>


                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">

                                </div>
                            </div>
                        </div>
<br><br>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <center><button type="submit" class="boton">
                                    Iniciar
                                </button></center>


                            </div>
                        </div>
                    </form>
                <!-- Remind Passowrd -->
                <div id="formFooter">

                </div>
            </div>
        </div>
    </body>
</html>
