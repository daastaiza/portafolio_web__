<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Menu</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">


    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
    'csrfToken' => csrf_token(),
]); ?>
    </script>
</head>
<body>
    <style type="text/css">
        .colornavbar{
          
            background-color: #fb4b4b!important;
        }
        .colortitle{
            color: white;
        }
        .colortitle:active{
           background-color: white!important;
        }
        .colortitle:hover{
           background-color: white!important;
        }
        body{
            background-color: white!important;
        }
    </style>
    <div id="app">
        <nav class="navbar colornavbar navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand colortitle" href="{{ url('/') }}">
                        Portafolio
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle colortitle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Opciones <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        

                                        
                                        <a  href="{{ url('Administrador/Publicaciones') }}">Publicaciones</a>

                                        <a  href="{{ url('Administrador/Ciudad') }}">Ciudades</a>
                                        <a  href="{{ url('Administrador/Portafolio') }}">Portafolio</a>

                                        <a href="{{ url('Administrador/Estudios') }}">Estudios</a>

                                        <a href="{{ url('Administrador/Categoria') }}">Categorias</a>

                                        <a href="{{ url('Administrador/Secciones') }}">Sessiones</a>

                                        <a href="{{ url('Administrador/Informacion') }}">Informacion</a>

                                         <a href="{{ url('Administrador/Aportes') }}">Aporte a proyectos</a>
                                         <br>

                                         <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Cerrar sesion
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>

</body>
</html>
