<!DOCTYPE HTML>
<html>
    <head>
        <title>
            Portafolio personal
        </title>
        <meta charset="utf-8"/>
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <link href="{{asset('assets/css/main.css')}}" rel="stylesheet"/>
        <link href="{{asset('assets/css/sweetalert.css')}}" rel="stylesheet"/>
        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">  
        <style type="text/css">
       
        </style>  
    </head>
    <body>
        <!-- Header -->
        <header id="header">
            <div class="inner">
                <a class="logo" href="{{asset('index.html')}}">
                    <strong>
                        Diego
                    </strong>
                    Alejandro Astaiza
                </a>
                <nav id="nav">
                    <a href="{{route('login')}}"><span class="icon fa-user">
                        Ingresar
                    </a>
                    
                </nav>
                <a class="navPanelToggle" href="#navPanel">
                    <span class="fa fa-bars">
                    </span>
                </a>
            </div>
        </header>
        <!-- Banner -->
        <section id="banner">
            <div class="inner">
                <header>
                    <h1>
                        !Bienvenido a mi portafolio!
                    </h1>
                </header>
                <div class="flex ">
                   
                    <div>
                        <span class="icon fa-user">
                        </span>
                        <h3>
                            <a href="{{ url('informacion') }}">Informacion</a>
                        </h3>
                        <p>
                            informacion
                        </p>
                    </div>
                    
                    <div>
                        <span class="icon fa-gitlab">
                        </span>
                        <h3>
                            <a href="{{ url('aportes') }}">Aporte a proyectos</a>
                        </h3>
                        <p>
                            aportes realizados
                        </p>
                    </div>
                
                    <div>
                        <span class="icon fa-magic">
                        </span>
                       <h3>
                            <a  href="{{ url('portafolio') }}">Publicaciones</a>
                        </h3>
                        <p>
                            ingresar al blog
                        </p>
                    </div>
                    <div>
                        <span class="icon fa-graduation-cap">
                        </span>
                        <h3>
                            <a href="{{ url('estudios') }}">Estudios</a>
                        </h3>
                        <p>
                            estudios
                        </p>
                    </div>
                    
                </div>
                
            </div>
        </section>
        <!-- Three -->
        @include('flash::message')
        <section class="wrapper align-center" id="three">
            <div class="inner">
               <div class="flex flex-2">
                    <article>
                        <div class="image round">
                            <img alt="Pic 01" src="{{asset('images/pic01.jpg')}}"/>
                        </div>
                        <header>
                            <h3>
                                actualiza pls
                                <br/>
                            </h3>
                        </header>
                        <p>
                            Conocimientos en maquetacion web y diseño de formularios aplicando
                            <br>
                                aplicando Html5,CSS3 Jquery y framkeworks y Lenguajes de
                                <br/>
                                programacion orientados a objetos
                            </br>
                        </p>
                        <footer>
                            <a class="button" href="#" id="progra">
                                Me interesa
                            </a>
                            
                        </footer>
                    </article>
                    <article>
                        <div class="image round">
                            <img alt="Pic 01" src="{{asset('images/pic02.jpg')}}"/>
                        </div>
                        <header>
                            <h3>
                                Bases de datos
                                <br/>
                            </h3>
                        </header>
                        <p>
                            Conocimientos en diseño de bases de datos
                            <br/>
                            y en diagramas UML para la estructura
                            <br/>
                            del sistema
                        </p>
                        <footer>
                            <a class="button" href="#" id="datos">
                                Me interesa
                            </a>
                        </footer>
                    </article>
                </div>
            </div>
        </section>


        <footer id="footer">
            <p>Si tienes cualquier duda acerca de mis servicios de diseño web  quieres realizarme una propuesta o bien  necesitas solicitar presupuesto <br> no dudes en contactar conmigo a través del siguiente formulario. Incluye la máxima información para poder ofrecerte una respuesta <br> más concisa.

Agradezco sugerencias, consejos y oportunidades de negocio.
</p>
            <div class="inner">
                <h3>
                    Formulario de contacto
                </h3>
                {!! Form::open(['route'=>'index.store','method'=>'POST']) !!}
                    <div class="field half first">
                        <label for="name">
                            Nombre completo
                        </label>
                        {!!Form::text('name',null,['placeholder'=>'Nombre'])  !!}
                        
                    </div>
                    <div class="field half">
                        <label for="email">
                            Correo Electronico
                        </label>
                        {!!Form::text('email',null,['placeholder'=>'Email'])  !!}
                        </input>
                    </div>
                    <div class="field">
                        <label for="message">
                            Mensaje
                        </label>
                        {!!Form::textarea('mensaje',null,['placeholder'=>'Mensaje'])  !!}
                        </textarea>
                    </div>
                    
                        
                           {!! Form::submit('Enviar',['id'=>'enviar','class' =>'button alt']) !!}
                        
                    
                {!!Form::close()!!}
                
                
                        <span class="icon fa-user"></span>
                        <p>Nombre : Diego Alejandro Adtaiza Borja</p>
                        &emsp;
                        <span class="icon fa-home"></span>
                        <p>Direccion : cra 25 a # 32 a 76 sur</p>
                        <br>
                        <span class="icon fa-phone"></span>
                        <p>Telefono : 6941093 - 3003654431</p>
                        &emsp;
                        <span class="icon fa-send"></span>
                        <p>Correo Electronico : diegoalejandroastaiza@gmail.com</p>
                
            </div>
        </footer>
   
        <!-- Scripts -->
        <script src="{{asset('assets/js/jquery-3.2.1.min.js')}}">
        </script>
        <script src="{{asset('assets/js/skel.min.js')}}">
        </script>
        <script src="{{asset('assets/js/util.js')}}">
        </script>
        <script src="{{asset('assets/js/main.js')}}">
        </script>
        <script src="{{asset('assets/js/sweetalert.min.js')}}">
        </script>
         
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
    <script>
                                $(document).ready(function () {

                                    $("a[id=progra]").click(function(){
        swal({
                                    type:"info",
                                    title:"Informacion de contacto",
                                    
                                    text:"Hola si buscas quien  desarrolle una pagina web para tu empresa que administre contenidos o informativa por favor escribe un mensaje en el formulario de abajo",
                                    confirmButtonText: "aceptar",
                                    confirmButtonColor: "white", 
                                         
                                    animation: "slide-from-bottom"
                                    });
    });

                                                            $("a[id=datos]").click(function(){
        swal({
                                    type:"info",
                                    title:"Informacion de contacto",
                                    
                                    text:"Hola si buscas quien administre una base de datos o construya una base de datos o documentacion de sistemas de informacion  aplicando diagramas UML por favor escribe un mensaje en el formulario de abajo",
                                    confirmButtonText: "aceptar",
                                    confirmButtonColor: "white", 
                                         
                                    animation: "slide-from-bottom"
                                    });
    });


                                
                               });
                            </script>
    </body>
</html>
