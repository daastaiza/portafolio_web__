@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="container">
             <div class="container">
                <div class="panel-heading"><h2>Aportes</h2>
                </div>
                </div>
                </div>

                <div class="panel-body">
                    <div class="container-fluid">
                        <div class="card">
                <div class="card-header d-flex align-items-center">
                <div class="container"><h3 class="panel-title"><i class="fa fa-list"></i>


                  informacion de los aportes realizados</h3>
                  </div>
                  <br>
                </div>
                <div class="card-block">
                            <div class="panel-body">
                                @if(count($errors) >0)
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                 @include('flash::message')

                                <div class="table-responsive">
                                   <table class="table table-hover table-sm table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Identificador</th>
                                            <th>titulo</th>
                                            <th>descripcion</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($aportes as $aportes)
                                        <tr>
                                                <td>{{$aportes -> id }}</td>
                                                <td>{{$aportes -> titulo}}</td>
                                                <td>{{$aportes -> descripcion}}</td>
                                                
                                        </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <a ><button type="button" class="btn btn-success" data-toggle="modal"   data-target="#Crear">Nueva <i class="fa fa-fw fa-plus"></i></button></a><br><br>
                                </div>
                            </div>
                    </div>
                </div>

                        
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="Crear" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                        <!-- Modal content-->
                                    <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Registro de aporte</h4>
                                          </div>
                                          <div class="modal-body">


                                        {!! Form::open(['route' => 'Aportes.store', 'method' => 'POST']) !!}

                                        <div class="form-group">
                                            {!! Form::label('name', 'Titulo') !!}
                                            {!! Form::text('titulo', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'titulo del aporte']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('descripcion', 'Descripcion') !!}
                                            {!! Form::textarea('descripcion', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'descripcion del aporte realizado']) !!}
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::submit('Registrar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                                        </div>

                                        {!! Form::close() !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
@endsection
