@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        
                <div class="panel-heading">

                  <div class="container">
                     <div class="container">
                      
<h2>Informacion</h2>
                  </div> 
                  </div> 

                </div>

                <div class="panel-body">
                    <div class="container-fluid">
                        <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h3 class="panel-title"><i class="fa fa-list"></i>

                <div class="container">
                   
                   
                  
                  informacion de la informacion      
                   

                </div>
               
                   
              </h3>
                </div>
                <div class="card-block">
                            <div class="panel-body">
                                @if(count($errors) >0)
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @include('flash::message')

                                <div class="table-responsive">
                                   <table class="table table-hover table-sm table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Identificador</th>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>imagen</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($informacion as $informacion)
                                        <tr>
                                                <td >{{ $informacion -> id }}</td>
                                                <td>{{ $informacion -> nombre}}</td>
                                                <td>{{ $informacion -> descripcion}}</td>
                                                
                                                <td><img src="/storage/{{ $informacion -> image }}" width="100px"></td>
                                        </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <a ><button type="button" class="btn btn-success" data-toggle="modal"   data-target="#Crear">Nueva <i class="fa fa-fw fa-plus"></i></button></a><br><br>
                                </div>
                            </div>
                    </div>
                </div>

                          
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="Crear" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                        <!-- Modal content-->
                                    <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Registro de Peticion</h4>
                                          </div>
                                          <div class="modal-body">


                                        {!! Form::open(['route' => 'Informacion.store', 'method' => 'POST','files' => true]) !!}

                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('nombre', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'Ingrese el nombre']) !!}
                                        </div>
                                        
                                        <div class="form-group">
                                            {!! Form::label('description', 'Descripcion') !!}
                                            {!! Form::textarea('descripcion', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre de la peticion']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('name', 'Seleccione una imagen') !!}

                                            {!! Form::file('image') !!}
                                        </div>

                                        <div class="modal-footer">
                                            {!! Form::submit('Registrar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                                        </div>

                                        {!! Form::close() !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
@endsection
