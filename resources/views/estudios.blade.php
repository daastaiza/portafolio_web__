<!DOCTYPE HTML>
<!--
    Caminar by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
    <head>
        <title>Caminar by TEMPLATED</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="{{asset('assets/css/main_information.css')}}" />
    </head>
    <body>

        <!-- Header -->
            <header id="header">
                <div class="logo"><a href="#">Estudios <span>Diego Alejandro Astaiza Borja</span></a></div>
            </header>

        <!-- Main -->
            <section id="main">
                <div class="inner">

                <!-- One -->
                 
                <!-- Two -->
                    <section id="two" class="wrapper style2">
                        <header>
                            <h2>Informacion de educacion</h2>
                            <p>Diego Alejandro Astaiza Borja</p>
                        </header>
                        
                    </section>

               @foreach ($estudios as $estudios)

                    <section id="three" class="wrapper">
                        <div class="spotlight">
                            <div class="image flush"><img src="/storage/{{ $estudios -> image }}"  alt="" /></div>
                            <div class="inner">
                                <h3>{{ $estudios -> titulo}}</h3>
                                <p>{{ $estudios -> descripcion}}.</p>
                            </div>
                        </div>
                    </section>
                    <hr>
                     @endforeach

                </div>
            </section>
            

        <!-- Footer -->
            <footer id="footer">
                <div class="container">
                    <ul class="icons">
                        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                        <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
                    </ul>
                </div>
                <div class="copyright">
                    &copy; copyright. todos los derechos reservados. Aprendiz <a href="https://unsplash.com">SENA</a> diseñado  por<a href="https://templated.co">Diego Astaiza</a>
                </div>
            </footer>
            <br>
            <li><a href="http://localhost:8000/index" class="button special small fit"><span class="icon fa-user"> Regresar a inicio</a></li>

      

    </body>
</html>